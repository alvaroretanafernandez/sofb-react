
import React, {Component} from 'react';
import HomeComponent from '../../../app/home/containers/home.component';
import {
    Redirect,
    Route,
    Switch
} from 'react-router-dom';
import {SOFB_ROUTES} from '../../routes/sofb.routes';
import SofbLinksComponent from '../../../app/links/links.component';
import SofbVideosComponent from '../../../app/videos/videos.component';
import SofbMagazinesComponent from '../../../app/magazines/magazines.component';
import SofbMagazineDetailComponent from '../../../app/magazines/detail/magazine-detail.component';
import GalleryComponent from '../../../app/gallery/gallery.component';
import AboutComponent from '../../../app/about/about.component';
import AdvertComponent from '../../../app/advertising/advertising.component';
import ContactComponent from '../../../app/contact/contact.component';

const NoMatch = ({ location }) => (
    <div>
        <h3>No match for <code>{location.pathname}</code></h3>
    </div>
)

class SofbContentComponent extends Component {
    render() {
        return (
            <div>
                <Switch>
                    <Route exact path={ SOFB_ROUTES.HOME } component={HomeComponent} />
                    <Route  path={ SOFB_ROUTES.LINKS } component={SofbLinksComponent} />
                    <Route  path={ SOFB_ROUTES.VIDEOS } component={SofbVideosComponent} />
                    <Route  path={ SOFB_ROUTES.MAGAZINE_DETAIL } component={SofbMagazineDetailComponent} />
                    <Route  path={ SOFB_ROUTES.MAGAZINES } component={SofbMagazinesComponent} />
                    <Route  path={ SOFB_ROUTES.GALLERY } component={GalleryComponent} />
                    <Route  path={ SOFB_ROUTES.CONTACT } component={ContactComponent} />
                    <Route  path={ SOFB_ROUTES.ABOUT } component={AboutComponent} />
                    <Route  path={ SOFB_ROUTES.ADVERTISING } component={AdvertComponent} />
                    <Route component={NoMatch} />
                    <Redirect to="/" />
                </Switch>
            </div>
        );
    }
}

export default SofbContentComponent;