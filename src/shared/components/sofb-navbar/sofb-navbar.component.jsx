
import React, { Component } from 'react';
import { Nav, Navbar, NavItem } from 'react-bootstrap';
import { SOFB_STATIC } from '../../constants/sofb.constants';
import './sofb-navbar.scss';
import {withRouter} from 'react-router-dom'


class SofbNavbarComponent extends Component {
    constructor(props) {
        super(props);
        this.state = { menu: SOFB_STATIC.content.global.menu };

    }
    render() {
        return (<Navbar inverse fluid>
            <Navbar.Header>
                <Navbar.Brand>
                    <a href="/">
                        <img src="../../assets/img/sofb-mini.png" className="logo" alt="Stoked On Fixed Bikes Logo"/>
                    </a>
                </Navbar.Brand>
            </Navbar.Header>
            <Nav pullRight>
                {this.state.menu.map(it =>
                    <NavItem key={it.state}  href={it.state}>{it.title}</NavItem>
                )}
            </Nav>
        </Navbar>);
    }
}
const SofbNavbarWithRouter = withRouter(SofbNavbarComponent);

export default SofbNavbarWithRouter;