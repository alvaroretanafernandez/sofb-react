import renderer from 'react-test-renderer';
import SofbNavbarComponent from './sofb-navbar.component';
import React from 'react';

it('should create component', () => {
    const component = renderer.create(
        <SofbNavbarComponent></SofbNavbarComponent>,
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
    expect(tree.props.className).toBe('navbar navbar-inverse');
});
