import {Component} from 'react';
import React from 'react';
import SofbNavbarComponent from '../sofb-navbar/sofb-navbar.component';
import SofbBannerComponent from '../sofb-banner/sofb-banner.component';
import SofbPrefooterComponent from '../sofb-prefooter/sofb-prefooter.component';
import SofbFooterComponent from '../sofb-footer/sofb-footer.component';
import SofbContentComponent from '../sofb-content/sofb-content.component';

class SofbMainLayoutComponent extends Component {
    render() {
        console.log(this.props);
        return (
            <div>
                <SofbNavbarComponent></SofbNavbarComponent>
                <SofbContentComponent/>
                <SofbBannerComponent/>
                <SofbPrefooterComponent/>
                <SofbFooterComponent/>
            </div>
        );
    }
}

export default SofbMainLayoutComponent;