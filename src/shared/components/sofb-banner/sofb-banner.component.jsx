import React, {Component} from 'react';
import {Carousel} from 'react-bootstrap';
import {SOFB_STATIC} from '../../constants/sofb.constants';
import './sofb-banner.component.css';

class SofbBannerComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sofbAdSlides: SOFB_STATIC.content.global.banner.sofbAdSlides,
            poloAndBikeSlides: SOFB_STATIC.content.global.banner.poloAndBikeSlides,
            kappsteinSlides: SOFB_STATIC.content.global.banner.kappsteinSlides,
            bombtrackSlides: SOFB_STATIC.content.global.banner.bombtrackSlides
        }
    }

    render() {
        return (
            <div className="container-fluid sofb-banner">
                <div className="row">
                    <div className="col-xs-12 col-md-3">
                        <div className="carousel-sofb-small">
                            <Carousel interval={10000}>
                                {this.state.sofbAdSlides.map(it => <Carousel.Item key={it.img}><img src={it.img}
                                                                                                    alt="SOFB slide"/></Carousel.Item>)}
                            </Carousel>
                        </div>
                    </div>
                    <div className="col-xs-12 col-md-3">
                        <div className="carousel-sofb-small">
                            <Carousel interval={5000}>
                                {this.state.poloAndBikeSlides.map(it => <Carousel.Item key={it.img}><img src={it.img}
                                                                                                         alt="SOFB slide"/></Carousel.Item>)}
                            </Carousel>
                        </div>
                    </div>
                    <div className="col-xs-12 col-md-3">
                        <div className="carousel-sofb-small">
                            <Carousel interval={7000}>
                                {this.state.kappsteinSlides.map(it => <Carousel.Item key={it.img}><img src={it.img}
                                                                                                       alt="SOFB slide"/></Carousel.Item>)}
                            </Carousel>
                        </div>
                    </div>
                    <div className="col-xs-12 col-md-3">
                        <div className="carousel-sofb-small">
                            <Carousel interval={3000}>
                                {this.state.bombtrackSlides.map(it => <Carousel.Item key={it.img}><img src={it.img}
                                                                                                       alt="SOFB slide"/></Carousel.Item>)}
                            </Carousel>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default SofbBannerComponent;
