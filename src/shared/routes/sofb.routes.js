

export const SOFB_ROUTES = {
    HOME: '/',
    MAGAZINES: '/magazines',
    MAGAZINE_DETAIL: '/magazines/:id',
    GALLERY: '/gallery',
    VIDEOS: '/videos',
    LINKS: '/links',
    ABOUT: '/about',
    ADVERTISING: '/advertising',
    CONTACT: '/contact',
}