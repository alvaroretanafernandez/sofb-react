import {createEpicMiddleware} from 'redux-observable';
import rootEpic from './epics/index';
import reducer  from './reducers/index';
import { createStore, compose, applyMiddleware } from 'redux';

const epicMiddleware = createEpicMiddleware();
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(reducer, composeEnhancers(applyMiddleware(epicMiddleware)));
epicMiddleware.run(rootEpic);
export default store

