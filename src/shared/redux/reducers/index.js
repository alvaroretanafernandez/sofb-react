import {links} from './links.reducer';
import {videos} from './videos.reducer';
import {magazines, magazine} from './magazines.reducer';
import {combineReducers} from 'redux';

export default combineReducers({
    links,
    videos,
    magazines,
    magazine
});