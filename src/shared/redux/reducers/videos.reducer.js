import {
    FETCH_VIDEOS_SUCCESS,
    FETCH_VIDEOS_FAILED
} from '../../constants/sofb.constants';

const initialState = {
    videos: []
};
export const videos = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_VIDEOS_SUCCESS:
            return action.payload;
        case FETCH_VIDEOS_FAILED:
            return {
            };
        default:
            return state;
    }
};
