import {
    FETCH_MAGAZINES_SUCCESS,
    FETCH_MAGAZINES_FAILED,
    SELECT_MAGAZINE,
    FETCH_MAGAZINE_SUCCESS,
    FETCH_MAGAZINE_FAILED
} from '../../constants/sofb.constants';

const initialState = {
    magazines: [],
    magazine:{}
};
export const magazines = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_MAGAZINES_SUCCESS:
            return {
                magazines: action.payload
            };
        case FETCH_MAGAZINES_FAILED:
            return {
            };
        case SELECT_MAGAZINE:
            return {
                ...state,
                magazine: action.payload
            };
        default:
            return state;
    }
};

const initialStateMag = {
    magazine:{}
};
export const magazine = (state = initialStateMag, action) => {
    switch (action.type) {
        case FETCH_MAGAZINE_SUCCESS:
            return {
                magazine: action.payload
            };
        case FETCH_MAGAZINE_FAILED:
            return {
            };
        default:
            return state;
    }
};


