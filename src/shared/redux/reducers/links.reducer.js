import {
    FETCH_LINKS_SUCCESS,
    FETCH_LINKS_FAILED
} from '../../constants/sofb.constants';

const initialStateLinks = {};

export const links = (state = initialStateLinks, action) => {
    switch (action.type) {
        case FETCH_LINKS_SUCCESS:
            return action.payload;
        case FETCH_LINKS_FAILED:
            return {};
        default:
            return state;
    }
};


