import {fetchLinks, fetchMagazines, fetchVideos, fetchMagazine} from './sofb.epics';
import {combineEpics} from 'redux-observable';

export default combineEpics(
    fetchLinks,
    fetchVideos,
    fetchMagazines,
    fetchMagazine
);