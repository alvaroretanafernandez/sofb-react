import {sofbRestApiService} from '../../services/sofb.services';
import {
    fetchLinksFailed,
    fetchLinksSuccess, fetchMagazineFailed, fetchMagazinesFailed,
    fetchMagazinesSuccess, fetchMagazineSuccess,
    fetchVideosFailed,
    fetchVideosSuccess
} from '../actions/sofb.actions';
import {FETCH_LINKS, FETCH_MAGAZINE, FETCH_MAGAZINES, FETCH_VIDEOS} from '../../constants/sofb.constants';
import { of } from 'rxjs';
import {ofType} from 'redux-observable';
import { map, switchMap, catchError }from 'rxjs/operators';


export const fetchLinks = action$ => action$.pipe(
    ofType(FETCH_LINKS),
    switchMap(() =>
    sofbRestApiService.getSofbLinks()
        .pipe(
            map(data => { console.log(data.response);   return new fetchLinksSuccess(data.response)}),
            catchError(error => of(new fetchLinksFailed(error)))
        )
    )
);


export const fetchVideos = action$ => action$.pipe(
    ofType(FETCH_VIDEOS),
    switchMap(() =>
        sofbRestApiService.getSofbVideos()
            .pipe(
                map(links => new fetchVideosSuccess(links.response)),
                catchError(error => of(new fetchVideosFailed(error)))
            )
    )
);


export const fetchMagazines = action$ => action$.pipe(
    ofType(FETCH_MAGAZINES),
    switchMap(() =>
        sofbRestApiService.getSofbMagazines()
            .pipe(
                map(links => new fetchMagazinesSuccess(links.response)),
                catchError(error => of(new fetchMagazinesFailed(error)))
            )
    )
);

export const fetchMagazine = action$ => action$.pipe(
    ofType(FETCH_MAGAZINE),
    switchMap((action) =>
        sofbRestApiService.getSofbMagazine(action.payload)
            .pipe(
                map(data => { console.log(data.response);   return new fetchMagazineSuccess(data.response)}),
                catchError(error => of(new fetchMagazineFailed(error)))
            )
    )
);