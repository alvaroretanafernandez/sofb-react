import {
    FETCH_LINKS,
    FETCH_LINKS_FAILED,
    FETCH_LINKS_SUCCESS, FETCH_MAGAZINE, FETCH_MAGAZINE_FAILED, FETCH_MAGAZINE_SUCCESS,
    FETCH_MAGAZINES,
    FETCH_MAGAZINES_FAILED,
    FETCH_MAGAZINES_SUCCESS,
    FETCH_VIDEOS,
    FETCH_VIDEOS_FAILED,
    FETCH_VIDEOS_SUCCESS, SELECT_MAGAZINE
} from '../../constants/sofb.constants';

export const fetchLinks = () => ({
    type: FETCH_LINKS
});

export const fetchLinksSuccess = links => ({
    type: FETCH_LINKS_SUCCESS,
    payload: links
});

export const fetchLinksFailed = () => ({
    type: FETCH_LINKS_FAILED
});


export const fetchVideos = () => ({
    type: FETCH_VIDEOS,
});

export const fetchVideosSuccess = videos => ({
    type: FETCH_VIDEOS_SUCCESS,
    payload: videos
});

export const fetchVideosFailed = () => ({
    type: FETCH_VIDEOS_FAILED
});

export const fetchMagazines = () => ({
    type: FETCH_MAGAZINES,
});

export const fetchMagazinesSuccess = magazines => ({
    type: FETCH_MAGAZINES_SUCCESS,
    payload: magazines
});

export const fetchMagazinesFailed = () => ({
    type: FETCH_MAGAZINES_FAILED
});

export const selectMagazine = magazine => ({
    type: SELECT_MAGAZINE,
    payload: magazine
});


export const fetchMagazine = (id) => ({
    type: FETCH_MAGAZINE,
    payload: id
});

export const fetchMagazineSuccess = magazine => ({
    type: FETCH_MAGAZINE_SUCCESS,
    payload: magazine
});

export const fetchMagazineFailed = () => ({
    type: FETCH_MAGAZINE_FAILED
});