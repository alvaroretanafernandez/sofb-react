import {ajax} from 'rxjs/observable/dom/ajax';


export class SofbRestApiService {
    /**
     * SOFB LINKS
     * @returns {Observable<any>}
     */
    getSofbLinks = () =>
        // (ajax.getJSON('https://api.stokedonfixedbikes.com/api/links'));
        (ajax({
            method: 'GET',
            url: 'https://api.stokedonfixedbikes.com/api/links',
            headers: {
                'Content-Type': 'application/json',
            },
        }));
    /**
     * SOFB VIDEOS
     * @returns {Observable<any>}
     */
    getSofbVideos = () => (ajax({
        method: 'GET',
        url: 'https://api.stokedonfixedbikes.com/api/videos',
        headers: {
            'Content-Type': 'application/json',
        },
    }));;
    /**
     * SOFB MAGAZINES
     * @returns {Observable<any>}
     */
    getSofbMagazines = () =>
        (ajax({
            method: 'GET',
            url: 'https://api.stokedonfixedbikes.com/api/magazines',
            headers: {
                'Content-Type': 'application/json',
            },
        }));;
    /**
     * SOFB MAGAZINE
     * @param _id
     * @returns {Observable<any>}
     */
    getSofbMagazine = (_id) => (ajax({
        method: 'GET',
        url: 'https://api.stokedonfixedbikes.com/api/magazines/' +_id,
        headers: {
            'Content-Type': 'application/json',
        },
    }));;

}

const sofbRestApiService = new SofbRestApiService();
export { sofbRestApiService }
