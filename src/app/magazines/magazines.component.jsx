import React, {Component} from 'react';
import {connect} from 'react-redux';
import {fetchMagazines, selectMagazine} from '../../shared/redux/actions/sofb.actions';
import './magazines.component.scss';

class SofbMagazinesComponent extends Component {
    constructor(props) {
        super(props);
        this.state = { magazines: [] };
        this.props.fetchMagazines();
        this.handleClick =this.handleClick.bind(this);

    }
    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.magazines.magazines.length > 0) {
            return {
                magazines: nextProps.magazines.magazines,
                loaded: true,
            };
        }
        return null;
    }
    handleClick(it) {
        this.props.selectMagazine(it);
        window.location.href=this.props.match.url + '/' + it.id;
    }
    render() {
        return (

            <div className="container-fluid sofb-magazines">
                <div className="row sofb-subheader">
                    <div className="col-12 p-2">
                        <h2>Magazines</h2>
                    </div>
                </div>

                <div className="row justify-content-center p-3">
                    <div className="col-lg-12 wrapper-items">
                        <div className="wrap-magazine-items">
                            {this.state.magazines.length > 0 ? this.state.magazines.map(it =>
                                <div className="magazine-item  col-6 col-md-4 col-lg-2" key={it.id}>
                                    <div className="item-inner">
                                        <img src={it.img} alt="Magazine SOFB"/>
                                            <div className="overlay" >
                                            <span  className="preview btn btn-outline-dark " onClick={() => this.handleClick(it)} rel="prettyPhoto">Read</span>
                                    </div>
                                </div>
                                </div>
                            ) : null}
            </div>
        </div>
    </div>
</div>
        );
    }
}


const mapStateToProps = state => ({
    magazines: state.magazines,
    selected: state.selected
});
const mapDispatchToProps = {
    fetchMagazines,
    selectMagazine
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SofbMagazinesComponent);
