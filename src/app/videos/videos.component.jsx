import React, {Component} from 'react';
import {connect} from 'react-redux';
import {fetchVideos} from '../../shared/redux/actions/sofb.actions';
import './videos.component.css'

class SofbVideosComponent extends Component {
    constructor(props) {
        super(props);
        this.state = { videos: [] };
        this.props.fetchVideos();
        this.handleClick =this.handleClick.bind(this);

    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.videos.length > 0) {
            return {
                videos: nextProps.videos,
                loaded: true,
            };
        }
        return null;
    }

    handleClick(it) {
            console.log('ds');
    }

    render() {
        return (
            <div>
                <div className="container-fluid">
                    <div className="row justify-content-center p-4">
                        <div className="col-lg-12">
                            <div className="portfolio-items">
                                {this.state.videos.length > 0 ? this.state.videos.map(it =>
                                    <li key={it._id} className="portfolio-item  col-md-2 col-xs-6" onClick={() => this.handleClick(it)}>
                                        <div className="item-inner">
                                            <img src={it.preview} alt={it.title}/>
                                            <p className="title">
                                                <span className="text-muted pull-left">{it.title}</span>
                                            </p>
                                        </div>
                                    </li>
                                ) : null}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    videos: state.videos
});
const mapDispatchToProps = {
    fetchVideos
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SofbVideosComponent);
