import React, { Component } from 'react';
import './App.scss';
import SofbNavbarWithRouter from '../shared/components/sofb-navbar/sofb-navbar.component';
import SofbContentComponent from '../shared/components/sofb-content/sofb-content.component';
import SofbBannerComponent from '../shared/components/sofb-banner/sofb-banner.component';
import SofbPrefooterComponent from '../shared/components/sofb-prefooter/sofb-prefooter.component';
import SofbFooterComponent from '../shared/components/sofb-footer/sofb-footer.component';


class App extends Component {
  render() {
    return (
        <div>
            <SofbNavbarWithRouter></SofbNavbarWithRouter>

            <SofbContentComponent/>
            <SofbBannerComponent/>
            <SofbPrefooterComponent/>
            <SofbFooterComponent/>
        </div>
    );
  }
}

export default App;
