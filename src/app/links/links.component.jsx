import React, {Component} from 'react';
import {connect} from 'react-redux';
import {fetchLinks} from '../../shared/redux/actions/sofb.actions';
import './links.component.css'
import SofbLinkItemComponent from './components/link-item.component';

class SofbLinksComponent extends Component {
    constructor(props) {
        super(props);
        this.state = { links: [] };
        this.props.fetchLinks();

    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.links.length > 0) {
            return {
                links: nextProps.links,
                loaded: true,
            };
        }
        return null;
    }

    render() {
        return (
            <div>
                <div className="container-fluid">
                    <div className="row sofb-subheader">
                        <div className="col-12 p-2">
                            <h2>Links</h2>
                        </div>
                    </div>
                    <div className="row justify-content-center p-4">
                        <div className="col-lg-12">
                            <div className="links-wrap">
                                {this.state.links.length > 0 ? this.state.links.map(it =>
                                    <div className="col-md-2" key={it._id}>
                                        <SofbLinkItemComponent item={it}></SofbLinkItemComponent>
                                    </div>
                                ) : null}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    links: state.links
});
const mapDispatchToProps = {
    fetchLinks
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SofbLinksComponent);
